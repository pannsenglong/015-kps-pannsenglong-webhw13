import React, { Component } from 'react'
import {Row, Col} from 'react-bootstrap'
import { Button, Form} from 'react-bootstrap'
import Axios from 'axios'

//let fakeDAta = {ID: '001', TITLE: 'Please comeback', DESCRIPTION: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Et incidunt temporibus assumenda a, quas culpa! Porro placeat odio corporis quaerat debitis exercitationem maxime aspernatur. Inventore, sunt totam. Rem, dolore saepe", CREATED_DATE: "10-06-2020", IMAGE: "https://dmu9n6at1rzsj.cloudfront.net/original/2X/c/ca4dd513aef15b76c3432bd61c5af5d08c82e285.jpeg"}

export default class Update extends Component {
    constructor(props){
        super(props)
        this.state = {
            getDataByID: {},
            isTitle: true,
            isDesc: true,
            isIma: true
        }
    }

    componentWillMount() {
        let id = this.props.match.params.id;
        console.log(id);
        
        Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
            .then((res) => { 
            console.log(res.data.DATA);
            this.setState({ 
              getDataByID: res.data.DATA,   
            });
            })
            .catch((error) => {
                console.log(error); 
            });

        // this.setState({
        //     getDataByID: fakeDAta,
        //     TITLE: fakeDAta.TITLE,
        //     DESCRIPTION: fakeDAta.DESCRIPTION,
        //     IMAGE: fakeDAta.IMAGE
        // })
        
    }


    updateArticle = () =>{
        if(this.state.isTitle === true && this.state.isDesc === true && this.state.isImg === true)
        {
            let id = this.props.match.params.id;
            let title = document.getElementById("title").value
            let desc = document.getElementById("desc").value
            let img = document.getElementById("img").value
            const data = {TITLE: title+"ok", DESCRIPTION: desc+"ok", IMAGE: img}   
            console.log(id);
            Axios.put(`http://110.74.194.124:15011/v1/api/articles/${id}`,data )
                .then((res) =>{
                    console.log(res.data.DATA);
                    
                })
                .catch(error => {
                    console.log(error);
                }) 
                alert("Update Succecfully.")
                window.history.back()
              
        }
        else{
              alert("Update Failed")
        }
    }


 handleTitle = (event) =>
    { 
        this.setState({
            TITLE: event.target.value
        })
        let errorTitle = document.getElementById("errorTitle")
        let title = document.getElementById("title")
        if(title.value === '')
        {    
            errorTitle.innerHTML = ("Title is required.")
            console.log(errorTitle.value);
            this.setState({
                isTitle: false
            })
        }
        else{
            errorTitle.innerHTML = ("")  
            this.setState({
                isTitle: true
            })
            console.log(title.value, this.state.isTitle);
        }
    }


    handleDesc = (event) =>
    {
        this.setState({
            DESCRIPTION: event.target.value
        })
        let errorDesc = document.getElementById("errorDesc")
        let desc = document.getElementById("desc")
        if(desc.value === '')
        {    
            errorDesc.innerHTML = ("Description is required")
            console.log(errorDesc.value);
            this.setState({
                isDesc: false
            })
        }
        else{
            errorDesc.innerHTML = ("")  
            this.setState({
                isDesc: true
            })
        }

    }

    handleImg = (event) =>
    {
        this.setState({
            IMAGE: event.target.value
        })
        let errorImg = document.getElementById("errorImg")
        let img = document.getElementById("img")
        if(img.value === '')
        {    
            errorImg.innerHTML = ("Description is required")
            console.log(errorImg.value);
            this.setState({

                isImg: false
            })
        }
        else{
            errorImg.innerHTML = ("")  
            this.setState({
                isImg: true
            })
        }

    }

   
    render() {
        return (
            <div className="container" style={{marginTop: '30px'}}>
                <Row>
                    <Col md={12}>
                        <Form>
                            <Form.Group controlId="formBasicText">
                                <Form.Label style={{fontWeight: 'bold'}}>Title: </Form.Label>
                                <span id="errorTitle" className="error" style={{color: 'red', paddingLeft: "20px"}}></span>
                                <Form.Control id="title" type="text" placeholder="Enter Title" onChange={this.handleTitle} value={this.state.TITLE} />
                            </Form.Group>

                            <Form.Group controlId="formBasicText">
                                <Form.Label style={{fontWeight: 'bold'}}>Description: </Form.Label>
                                <span id="errorDesc" className="error" style={{color: 'red', paddingLeft: "20px"}}></span>
                                <Form.Control id="desc" type="text" placeholder="Enter Description" onChange={this.handleDesc} value={this.state.DESCRIPTION}/>
                            </Form.Group>
                            <Form.Group controlId="formBasicText">
                                <Form.Label style={{fontWeight: 'bold'}}>Image Link: </Form.Label>
                                <span id="errorImg" className="error" style={{color: 'red', paddingLeft: "20px"}}></span>
                                <Form.Control id="img" type="text" placeholder="Enter Image Link" onChange={this.handleImg} value={this.state.IMAGE}/>
                            </Form.Group>

                            <img style={{width: '100%', marginBottom: '20px', borderRadius: '20px'}} src={this.state.IMAGE} alt="img"/> <br></br>
                            <Button variant="primary" onClick={this.updateArticle}>
                                Submit
                            </Button>
                            <div style={{height: '100px'}}></div>
                        </Form>
                    </Col>
                </Row>
        </div>
        )
    }
}


