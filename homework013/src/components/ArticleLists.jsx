import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Axios from 'axios'
import {Link} from 'react-router-dom'
import {Button} from 'react-bootstrap'

export default class ArticleLists extends Component {

    delete = () =>{
        let yesNo = window.confirm("Are you sure ?");
        if(yesNo){
            Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${this.props.data.ID}`)
            .then(res => {
                console.log(res);
            })
            alert("Articles delete successfully.")
            this.setState({
            })
        }else{
            alert("Delete has been canceled.")
        }
    }

    convertDate = (time) => {
        let day = time.substring(6, 8);
        let month = time.substring(4, 6);
        let year = time.substring(0, 4);
        let date = [day, month, year];
        // console.log(date)
        return date.join("-");
      };
    
    render() {
        return (
                <tr>
                    <td>{this.props.data.ID}</td>
                    <td>{this.props.data.TITLE}</td>
                    <td>{this.props.data.DESCRIPTION}</td>
                    <td>{this.convertDate(this.props.data.CREATED_DATE)}</td>
                    <td><img style={{width: '150px'}} src={this.props.data.IMAGE} alt="img"/></td>
                    <td style={{padding: '10px', display: 'flex' , justifyContent: 'center'}}>
                        <Link to={`/view/${this.props.data.ID}`}><Button style={{margin: '10px'}} variant="primary" >VIEW</Button></Link>
                        <Link ><Button style={{margin: '10px'}} variant="danger" onClick={this.delete}>DELETE</Button></Link>
                        <Link to={`/update/${this.props.data.ID}`}><Button style={{margin: '10px'}} variant="success"> EDIT </Button></Link>                       
                    </td>
                </tr>        
        )
    }
}
