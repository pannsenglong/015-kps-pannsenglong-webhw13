import React, { Component } from 'react'
import Axios from "axios";
import {Card}  from 'react-bootstrap'

export default class View extends Component {
    constructor(props){
        super(props)
        this.state = {
            getDataByID: {}
        }
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        console.log(id);
        
        Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
          .then((res) => { console.log(res.data.DATA);
            this.setState({ 
              getDataByID: res.data.DATA,   
            });
          })
          .catch((error) => {
            console.log(error);
          });
    }

    render() {
        let img =this.state.getDataByID.IMAGE
        console.log(img);
        return (
            <Card className="container" style={{marginTop: '50px', boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)', padding: '50px'}}>
                <div  style={{width: "100%" ,backgroundImage: "https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png"}}></div>
                <Card.Img style={[]}  variant="top" src={(img === null || img === 'string') ? "https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png": img} alt="img"/>
                <Card.Body style={{marginTop: '20px'}}>
                    <Card.Title style={{fontWeight: "bold", fontSize: '50px'}}> {this.state.getDataByID.TITLE}</Card.Title>
                    <Card.Text>{this.state.getDataByID.DESCRIPTION}</Card.Text>
                </Card.Body>
            </Card>
        )
    }
}