import React from 'react'
import {Row, Col} from 'react-bootstrap'
import { Button, Form} from 'react-bootstrap'
import Axios from 'axios'


export default function Add() {
    return (
        <div className="container" style={{marginTop: '30px'}}>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group controlId="formBasicText">
                            <Form.Label style={{fontWeight: 'bold'}}>Title: </Form.Label>
                            <span id="errorTitle" className="error" style={{color: 'red', paddingLeft: "20px"}}></span>
                            <Form.Control id="title" type="text" placeholder="Enter Title" onChange={handleTitle}/>
                        </Form.Group>

                        <Form.Group controlId="formBasicText">
                            <Form.Label style={{fontWeight: 'bold'}}>Description: </Form.Label>
                            <span id="errorDesc" className="error" style={{color: 'red', paddingLeft: "20px"}}></span>
                            <Form.Control id="desc" type="text" placeholder="Enter Description" onChange={handleDesc}/>
                        </Form.Group>
                        <Form.Group controlId="formBasicText">
                            <Form.Label style={{fontWeight: 'bold'}}>Image Link: </Form.Label>
                            <span id="errorImg" className="error" style={{color: 'red', paddingLeft: "20px"}}></span>
                            <Form.Control id="img" type="text" placeholder="Enter Image Link" onChange={handleImg}/>
                        </Form.Group>
                        <Button variant="primary" onClick={postArticle}>
                            Submit
                        </Button>
                    </Form>
                </Col>
                <Col md={4} style={{marginTop: '35px'}}>
                    <input type="file"/>
                </Col>
            </Row>
        </div>
    )
}







let isTitle = false;
let isDesc = false;
let isImg  =false;


const handleTitle = () =>
    
    { 
        let errorTitle = document.getElementById("errorTitle")
        let title = document.getElementById("title")
        if(title.value === '')
        {    
            errorTitle.innerHTML = ("Title is required.")
            console.log(errorTitle.value);
            isTitle = false;
        }
        else{
            errorTitle.innerHTML = ("")  
            isTitle = true
            console.log(errorTitle.innerHTML, isTitle);
        }
    }


    const handleDesc = () =>
    {
        let errorDesc = document.getElementById("errorDesc")
        let desc = document.getElementById("desc")
        if(desc.value === '')
        {    
            errorDesc.innerHTML = ("Description is required")
            console.log(errorDesc.value);
            isDesc = false;
        }
        else{
            errorDesc.innerHTML = ("")  
            isDesc = true
        }

    }

    const handleImg = () =>
    {
        let errorImg = document.getElementById("errorImg")
        let img = document.getElementById("img")
        if(img.value === '')
        {    
            errorImg.innerHTML = ("Description is required")
            console.log(errorImg.value);
            isImg = false;
        }
        else{
            errorImg.innerHTML = ("")  
            isImg = true;
        }

    }

    const postArticle = () => {
        if(isTitle === true && isDesc === true && isImg === true)
        {
            let title = document.getElementById("title").value
            let desc = document.getElementById("desc").value
            let img = document.getElementById("img").value
            console.log(title, desc, img);
            const data = {TITLE: title, DESCRIPTION: desc, IMAGE: img}        
            Axios.post("http://110.74.194.124:15011/v1/api/articles", data)
            .then((res) => {
                console.log(res); 
            })
            alert("Upload Successfully")
            isTitle = isDesc = isImg = false; 
            window.history.back();   
        }else{
            alert("Upload failed")
        }
    }