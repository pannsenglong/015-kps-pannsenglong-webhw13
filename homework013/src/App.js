import React from 'react';
import './App.css';

import {BrowserRouter as Router,Switch , Route} from 'react-router-dom'
import Add from './components/Add';
import Articles from './components/Articles';
import View from './components/View'
import Update from './components/Update'
import {Button, Navbar, Nav, NavDropdown, Form, FormControl} from 'react-bootstrap'

function App() {
  return (
      <Router>
            <div>
              <Navbar  bg="light" expand="lg">
                      <Navbar.Brand  href="#home">React-Bootstrap</Navbar.Brand>
                      <Navbar.Toggle aria-controls="basic-navbar-nav" />
                      <Navbar.Collapse id="basic-navbar-nav">
                          <Nav className="mr-auto">
                          <Nav.Link href="#home">Home</Nav.Link>
                          <Nav.Link href="#link">Link</Nav.Link>
                          <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                              <NavDropdown.Divider />
                              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                          </NavDropdown>
                          </Nav>
                          <Form inline>
                          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                          <Button variant="outline-success">Search</Button>
                          </Form>
                      </Navbar.Collapse>
              </Navbar>
            </div>

            <Switch>
                <Route exact path="/" component={Articles}/>
                <Route path="/add" component={Add}/>
                <Route path="/view/:id" component={View} />
                <Route path="/update/:id" component={Update}/>
            </Switch>
      </Router>
    
  );
}

export default App;
